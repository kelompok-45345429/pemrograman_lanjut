<!DOCTYPE html>
<html>
<head>
  <title>JAHANO Barbershop</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>SELAMAT DATANG DI JAHANO BARBERSHOP</h1>
    <nav>
      <ul>
        <li><a href="index.php">BERANDA</a></li>
        <li><a href="service.php">LAYANAN</a></li>
        <li><a href="about.php">ABOUT</a></li>
        <li><a href="contact.php">KONTAK</a></li>
        <li><a href="booking.php">BOOKING</a></li>
        <li><a href="view_booking.php">LIHAT BOOKING</a></li>
        <li><a href="login.php">login admin</a></li>
      </ul>
    </nav>
  </header>

  <section id="banner">
    <img src="1.jpg" alt="XYZ Barbershop" width="900" height="500">
  </section>

  <section id="welcome-message">
    <h2>SELAMAT DATANG DI JAHANO BARBERSHOP</h2>
    <p>salah satu barbershop terbaik di indonesia yang bisa bersaing dengan para barbershop ternama di indonesia dengan segala itunya</p>
  </section>

  <footer>
    <p>&copy; JAHANO BARBERSHOP</p>
  </footer>
</body>
</html>
