<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "barbershop";

if (isset($_GET['id'])) {
  $booking_id = $_GET['id'];

  try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("DELETE FROM bookings WHERE id = :id");
    $stmt->bindParam(':id', $booking_id);
    $stmt->execute();
    header("Location: view_booking_admin.php");
    exit();
  } catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}
