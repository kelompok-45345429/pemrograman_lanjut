<!DOCTYPE html>
<html>
<head>
  <title>About Us - Barbershop</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>JAHANO Barbershop</h1>
    <nav>
      <ul>
      <li><a href="index.php">BERANDA</a></li>
        <li><a href="service.php">LAYANAN</a></li>
        <li><a href="about.php">ABOUT</a></li>
        <li><a href="contact.php">KONTAK</a></li>
        <li><a href="booking.php">BOOKING</a></li>
        <li><a href="view_booking.php">LIHAT BOOKING</a></li>
      </ul>
    </nav>
  </header>

  <section id="about-content">
    <div class="about-image">
      <img src="3.jpg" alt="Barber at Work">
    </div>
    <div class="about-description">
      <h2>selamat datang di JAHANO Barbershop</h2>
      <p>Selamat datang di Jahano Barbershop - Destinasi Terkenal untuk Pria yang Mengutamakan Grooming

          Jahano Barbershop adalah sebuah barbershop yang telah lama berdiri dan telah membangun reputasi yang tak tertandingi dalam dunia grooming. Dengan pengalaman bertahun-tahun, Jahano Barbershop telah menjadi ikon bagi para pria yang mencari layanan grooming berkualitas tinggi dan perawatan rambut yang mewah. Berikut adalah deskripsi mengenai barbershop yang sangat berpengalaman dan terkenal ini:

          Keahlian dan Konsistensi: Jahano Barbershop membanggakan diri dalam keahlian para tukang cukur profesional mereka. Para tukang cukur ini telah mengasah keterampilan mereka selama bertahun-tahun dan selalu mengikuti tren terbaru dalam dunia grooming. Konsistensi dalam memberikan hasil potongan rambut yang luar biasa menjadi ciri khas barbershop ini.

          Layanan Personalisasi: Jahano Barbershop memahami bahwa setiap pelanggan memiliki kebutuhan yang unik. Oleh karena itu, mereka menawarkan layanan personalisasi untuk memastikan setiap pelanggan mendapatkan hasil yang sesuai dengan preferensi mereka. Mulai dari potongan rambut klasik hingga gaya rambut modern, barbershop ini mampu menciptakan penampilan yang sesuai dengan kepribadian dan gaya hidup pelanggan.

          Suasana yang Menyenangkan: Begitu Anda masuk ke Jahano Barbershop, Anda akan langsung merasakan suasana yang ramah dan nyaman. Dekorasi interior yang elegan dan hangat menciptakan lingkungan yang sempurna untuk relaksasi sambil menikmati pelayanan grooming berkualitas tinggi.

          Produk Perawatan Berkualitas: Jahano Barbershop hanya menggunakan produk perawatan rambut dan grooming yang berkualitas tinggi. Mereka menyediakan rangkaian produk premium yang diformulasikan khusus untuk memenuhi kebutuhan perawatan rambut dan kulit pria.

          Komitmen terhadap Kepuasan Pelanggan: Jahano Barbershop selalu berusaha memberikan pelayanan terbaik untuk memastikan kepuasan pelanggan. Semua stafnya ramah, profesional, dan selalu siap memberikan saran yang berguna tentang tren dan perawatan rambut terkini.

            Jahano Barbershop adalah tempat yang penuh gaya dan elegan bagi para pria modern yang mengutamakan penampilan dan kesejahteraan mereka. Dengan pengalaman dan reputasi yang luar biasa, Jahano Barbershop adalah destinasi terdepan untuk semua kebutuhan grooming Anda.</p>
<p></p>
    </div>
  </section>

  <footer>
  <p>&copy; JAHANO BARBERSHOP</p>
  </footer>
</body>
</html>
