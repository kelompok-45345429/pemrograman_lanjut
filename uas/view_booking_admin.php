<!DOCTYPE html>
<html>
<head>
  <title>lihat pesanan - Barbershop</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>lihat pesanan</h1>
    <nav>
      <ul>
      <li><a href="index.php">Log out</a></li>
      </ul>
    </nav>
  </header>
  <section id="booking-list">
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "barbershop";

    try {
      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT * FROM bookings");
      $stmt->execute();
      $bookings = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($bookings) > 0) {
        echo "<table>";
        echo "<tr><th>Name</th><th>Email</th><th>Phone</th><th>Service</th><th>Date</th><th>Time</th><th>Action</th></tr>";
        foreach ($bookings as $booking) {
          echo "<tr>";
          echo "<td>" . htmlspecialchars($booking['name']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['email']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['phone']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['service']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['appointment_date']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['appointment_time']) . "</td>";
          echo "<td>";
          echo "<a href=\"edit_booking.php?id=" . $booking['id'] . "\">Edit</a> | ";
          echo "<a href=\"delete_booking.php?id=" . $booking['id'] . "\" onclick=\"return confirm('Are you sure you want to delete this booking?');\">Delete</a>";
          echo "</td>";
          echo "</tr>";
        }
        echo "</table>";
      } else {
        echo "<p>No bookings found.</p>";
      }

    } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
    $conn = null;
    ?>

  <section id="messages">
    <h2>semua pesan</h2>
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "barbershop";

    try {
      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT * FROM messages ORDER BY created_at DESC");
      $stmt->execute();
      $messages = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if ($messages) {
        echo "<ul>";
        foreach ($messages as $message) {
          echo "<li>";
          echo "<strong>Name:</strong> " . htmlspecialchars($message['name']) . "<br>";
          echo "<strong>Email:</strong> " . htmlspecialchars($message['email']) . "<br>";
          echo "<strong>Message:</strong> " . htmlspecialchars($message['message']);
          echo "</li>";
        }
        echo "</ul>";
      } else {
        echo "<p>No bookings yet.</p>";
      }

    } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
    $conn = null;
    ?>
  </section>
  <footer>
  <p>&copy; JAHANO BARBERSHOP</p>
  </footer>
</body>
</html>
