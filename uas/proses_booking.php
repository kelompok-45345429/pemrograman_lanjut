<?php
if (isset($_POST['submit'])) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $phone = $_POST['phone'];
  $service = $_POST['service'];
  $date = $_POST['date'];
  $time = $_POST['time'];

  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "barbershop";

  try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("INSERT INTO bookings (name, email, phone, service, appointment_date, appointment_time) VALUES (:name, :email, :phone, :service, :appointment_date, :appointment_time)");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':service', $service);
    $stmt->bindParam(':appointment_date', $date);
    $stmt->bindParam(':appointment_time', $time);
    $stmt->execute();

    echo "<p>Thank you, $name! Your appointment for $service on $date at $time has been booked successfully.</p>";

  } catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}
?>
