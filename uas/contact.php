<!DOCTYPE html>
<html>
<head>
  <title>Hubungi kami - Barbershop</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>kontak kami</h1>
    <nav>
      <ul>
      <li><a href="index.php">BERANDA</a></li>
        <li><a href="service.php">LAYANAN</a></li>
        <li><a href="about.php">ABOUT</a></li>
        <li><a href="contact.php">KONTAK</a></li>
        <li><a href="booking.php">BOOKING</a></li>
        <li><a href="view_booking.php">LIHAT BOOKING</a></li>
</ul>
    </nav>
  </header>

  <section id="contact-form">
    <h2>Send us a message</h2>
    <?php
    if (isset($_GET['success']) && $_GET['success'] == 1) {
      echo "<p class='success-message'>Thank you for your message. We will get back to you soon!</p>";
    }
    ?>
    <form method="post" action="proses_contact.php">
      <label for="name">Name:</label>
      <input type="text" name="name" required>
      <label for="email">Email:</label>
      <input type="email" name="email" required>
      <label for="message">Message:</label>
      <textarea name="message" rows="5" required></textarea>
      <input type="submit" name="submit" value="Send Message">
    </form>
  </section>

  <section id="messages">
    <h2>Recent Messages</h2>
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "barbershop";

    try {
      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT * FROM messages ORDER BY created_at DESC LIMIT 5");
      $stmt->execute();
      $messages = $stmt->fetchAll(PDO::FETCH_ASSOC);
       if ($messages) {
        echo "<ul>";
        foreach ($messages as $message) {
          echo "<li>";
          echo "<strong>Name:</strong> " . htmlspecialchars($message['name']) . "<br>";
          echo "<strong>Email:</strong> " . htmlspecialchars($message['email']) . "<br>";
          echo "<strong>Message:</strong> " . htmlspecialchars($message['message']);
          echo "</li>";
        }
        echo "</ul>";
      } else {
        echo "<p>No messages yet.</p>";
      }

    } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
    $conn = null;
    ?>
  </section>

  <footer>
  <p>&copy; JAHANO BARBERSHOP</p>
  </footer>
</body>
</html>
