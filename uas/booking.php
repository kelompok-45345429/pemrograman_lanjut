<!DOCTYPE html>
<html>
<head>
  <title>Booking - Barbershop</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>Booking</h1>
    <nav>
      <ul>
      <li><a href="index.php">BERANDA</a></li>
        <li><a href="service.php">LAYANAN</a></li>
        <li><a href="about.php">ABOUT</a></li>
        <li><a href="contact.php">KONTAK</a></li>
        <li><a href="booking.php">BOOKING</a></li>
        <li><a href="view_booking.php">LIHAT BOOKING</a></li>
      </ul>
    </nav>
  </header>

  <section id="booking-form">
    <?php include 'proses_booking.php'; ?>
    <form method="post" action="">
      <label for="name">Nama:</label>
      <input type="text" name="name" required>
      <label for="email">Email:</label>
      <input type="email" name="email" required>
      <label for="phone">Phone:</label>
      <input type="tel" name="phone" required>
      <label for="service">pilih layanan:</label>
      <select name="service" required>
        <option value="Haircut">Haircut</option>
        <option value="Beard Trim">Beard Trim</option>
        <option value="Hot Towel Shave">Hot Towel Shave</option>
        <option value="Facial Massage">Facial Massage</option>
      </select>
      <label for="date">pilih tanggal:</label>
      <input type="date" name="date" required>
      <label for="time">pilih waktu:</label>
      <input type="time" name="time" required>
      <input type="submit" name="submit" value="Book Now">
    </form>
  </section>

  <footer>
  <p>&copy; JAHANO BARBERSHOP</p>
  </footer>
</body>
</html>
