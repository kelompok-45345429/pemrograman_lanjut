<!DOCTYPE html>
<html>
<head>
  <title>JAHANO BARBERSHOP</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>Edit Booking</h1>
  </header>

  <section id="edit-booking-form">
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "barbershop";

    if (isset($_GET['id'])) {
      $booking_id = $_GET['id'];

      try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT * FROM bookings WHERE id = :id");
        $stmt->bindParam(':id', $booking_id);
        $stmt->execute();
        $booking = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$booking) {
          echo "<p>Booking not found.</p>";
        } else {
          ?>
          <form method="post" action="update_booking.php">
            <input type="hidden" name="id" value="<?php echo $booking['id']; ?>">
            <label for="name">Name:</label>
            <input type="text" name="name" value="<?php echo htmlspecialchars($booking['name']); ?>" required>
            <label for="email">Email:</label>
            <input type="email" name="email" value="<?php echo htmlspecialchars($booking['email']); ?>" required>
            <label for="phone">Phone:</label>
            <input type="tel" name="phone" value="<?php echo htmlspecialchars($booking['phone']); ?>" required>
            <label for="service">Select Service:</label>
            <select name="service" required>
              <option value="Haircut" <?php if ($booking['service'] === 'Haircut') echo 'selected'; ?>>Haircut</option>
              <option value="Beard Trim" <?php if ($booking['service'] === 'Beard Trim') echo 'selected'; ?>>Beard Trim</option>
              <option value="Hot Towel Shave" <?php if ($booking['service'] === 'Hot Towel Shave') echo 'selected'; ?>>Hot Towel Shave</option>
              <option value="Facial Massage" <?php if ($booking['service'] === 'Facial Massage') echo 'selected'; ?>>Facial Massage</option>
            </select>
            <label for="date">Select Date:</label>
            <input type="date" name="date" value="<?php echo $booking['appointment_date']; ?>" required>
            <label for="time">Select Time:</label>
            <input type="time" name="time" value="<?php echo $booking['appointment_time']; ?>" required>
            <input type="submit" name="submit" value="Update Booking">
          </form>
        <?php
        }

      } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
      }
      $conn = null;
    } else {
      echo "<p>Invalid booking ID.</p>";
    }
    ?>
  </section>

  <footer>
  <p>&copy; JAHANO BARBERSHOP</p>
  </footer>
</body>
</html>
