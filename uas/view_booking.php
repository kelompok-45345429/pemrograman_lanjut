<!DOCTYPE html>
<html>
<head>
  <title>View Bookings - Barbershop</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>View Bookings</h1>
    <nav>
      <ul>
      <li><a href="index.php">BERANDA</a></li>
        <li><a href="service.php">LAYANAN</a></li>
        <li><a href="about.php">ABOUT</a></li>
        <li><a href="contact.php">KONTAK</a></li>
        <li><a href="booking.php">BOOKING</a></li>
        <li><a href="view_booking.php">LIHAT BOOKING</a></li>
      </ul>
    </nav>
  </header>

  <section id="booking-list">
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "barbershop";

    try {
      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT * FROM bookings");
      $stmt->execute();
      $bookings = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($bookings) > 0) {
        echo "<table>";
        echo "<tr><th>Name</th><th>Email</th><th>Phone</th><th>Service</th><th>Date</th><th>Time</th><th>Action</th></tr>";
        foreach ($bookings as $booking) {
          echo "<tr>";
          echo "<td>" . htmlspecialchars($booking['name']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['email']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['phone']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['service']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['appointment_date']) . "</td>";
          echo "<td>" . htmlspecialchars($booking['appointment_time']) . "</td>";
          echo "</tr>";
        }
        echo "</table>";
      } else {
        echo "<p>No bookings found.</p>";
      }

    } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
    $conn = null;
    ?>
  </section>

  <footer>
  <p>&copy; JAHANO BARBERSHOP</p>
  </footer>
</body>
</html>
