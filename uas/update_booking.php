<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "barbershop";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['service']) && isset($_POST['date']) && isset($_POST['time'])) {
    $booking_id = $_POST['id'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $service = $_POST['service'];
    $date = $_POST['date'];
    $time = $_POST['time'];

    try {
      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("UPDATE bookings SET name = :name, email = :email, phone = :phone, service = :service, appointment_date = :date, appointment_time = :time WHERE id = :id");
      $stmt->bindParam(':name', $name);
      $stmt->bindParam(':email', $email);
      $stmt->bindParam(':phone', $phone);
      $stmt->bindParam(':service', $service);
      $stmt->bindParam(':date', $date);
      $stmt->bindParam(':time', $time);
      $stmt->bindParam(':id', $booking_id);
      $stmt->execute();

      header("Location: view_booking_admin.php?id=" . $booking_id);
      exit;
    } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
    $conn = null;
  } else {
    echo "<p>Invalid input data. Please make sure all fields are filled.</p>";
  }
} else {
  echo "<p>Invalid request method. Please use the form to submit data.</p>";
}
?>
