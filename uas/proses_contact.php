<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "barbershop";

  $name = $_POST['name'];
  $email = $_POST['email'];
  $message = $_POST['message'];

  try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("INSERT INTO messages (name, email, message) VALUES (:name, :email, :message)");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':message', $message);
    $stmt->execute();
    header("Location: contact.php?success=1");
    exit();

  } catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
} else {
  header("Location: contact.php");
  exit();
}
?>
